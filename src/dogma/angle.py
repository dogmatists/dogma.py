# This is free and unencumbered software released into the public domain.

class Angle:
    """An angle."""

    radians: float

    def __init__(self, radians: float) -> None:
        self.radians = radians
