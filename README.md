# Dogma for Python

[![Project license](https://img.shields.io/badge/license-Public%20Domain-blue.svg)](https://unlicense.org)
[![PyPI package](https://img.shields.io/pypi/v/dogma.py.svg)](https://pypi.org/project/dogma-py/)
